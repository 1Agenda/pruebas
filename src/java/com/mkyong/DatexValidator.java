package com.mkyong;

import entidad.Lugares;
import java.util.Date;
import java.util.Vector;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;

@FacesValidator("datexValidator")
public class DatexValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {

        Date date = (Date) value;

        HtmlSelectOneMenu uiInputConfirmCapcity = (HtmlSelectOneMenu) component.getAttributes().get("type");
        Object duracion = uiInputConfirmCapcity.getSubmittedValue();

        if (duracion == null) {
            duracion = uiInputConfirmCapcity.getValue();
        }

        System.out.println("yo soy el tipo" + duracion);
        System.out.println("yo soy la fecha" + date);

    }

}
