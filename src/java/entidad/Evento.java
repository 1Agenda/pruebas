/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "evento", catalog = "prueba", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evento.findAll", query = "SELECT e FROM Evento e"),
    @NamedQuery(name = "Evento.findByIdevento", query = "SELECT e FROM Evento e WHERE e.idevento = :idevento"),
    @NamedQuery(name = "Evento.findByFechaInicioRegisto", query = "SELECT e FROM Evento e WHERE e.fechaInicioRegisto = :fechaInicioRegisto"),
    @NamedQuery(name = "Evento.findByFechaFinalRegistro", query = "SELECT e FROM Evento e WHERE e.fechaFinalRegistro = :fechaFinalRegistro")})
public class Evento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idevento")
    private Integer idevento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaInicioRegisto")
    @Temporal(TemporalType.DATE)
    private Date fechaInicioRegisto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaFinalRegistro")
    @Temporal(TemporalType.DATE)
    private Date fechaFinalRegistro;
    @JoinColumn(name = "agenda_idAgenda", referencedColumnName = "idAgenda")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Agenda agendaidAgenda;

    public Evento() {
    }

    public Evento(Integer idevento) {
        this.idevento = idevento;
    }

    public Evento(Integer idevento, Date fechaInicioRegisto, Date fechaFinalRegistro) {
        this.idevento = idevento;
        this.fechaInicioRegisto = fechaInicioRegisto;
        this.fechaFinalRegistro = fechaFinalRegistro;
    }

    public Integer getIdevento() {
        return idevento;
    }

    public void setIdevento(Integer idevento) {
        this.idevento = idevento;
    }

    public Date getFechaInicioRegisto() {
        return fechaInicioRegisto;
    }

    public void setFechaInicioRegisto(Date fechaInicioRegisto) {
        this.fechaInicioRegisto = fechaInicioRegisto;
    }

    public Date getFechaFinalRegistro() {
        return fechaFinalRegistro;
    }

    public void setFechaFinalRegistro(Date fechaFinalRegistro) {
        this.fechaFinalRegistro = fechaFinalRegistro;
    }

    public Agenda getAgendaidAgenda() {
        return agendaidAgenda;
    }

    public void setAgendaidAgenda(Agenda agendaidAgenda) {
        this.agendaidAgenda = agendaidAgenda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idevento != null ? idevento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evento)) {
            return false;
        }
        Evento other = (Evento) object;
        if ((this.idevento == null && other.idevento != null) || (this.idevento != null && !this.idevento.equals(other.idevento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Evento[ idevento=" + idevento + " ]";
    }
    
}
